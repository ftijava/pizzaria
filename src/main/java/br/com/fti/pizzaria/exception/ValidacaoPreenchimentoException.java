package br.com.fti.pizzaria.exception;

public class ValidacaoPreenchimentoException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public ValidacaoPreenchimentoException(String message) {
		super(message);
	}

}
