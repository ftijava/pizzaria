package br.com.fti.pizzaria.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.com.fti.pizzaria.model.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

	Cliente findById(Integer id);
	
	@Query("select c from Cliente c where c.nome like %?1")
	List<Cliente> findByNome(String nome);

}
