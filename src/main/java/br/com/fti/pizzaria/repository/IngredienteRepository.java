package br.com.fti.pizzaria.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.fti.pizzaria.model.Ingrediente;

public interface IngredienteRepository extends CrudRepository<Ingrediente, Integer> {
	
	List<Ingrediente> findByDescricao(String descricao);
	

}
