package br.com.fti.pizzaria.model;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "cliente")
public class Cliente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Getter private Integer id;
	
	@Column(name = "nome")
	@Getter @Setter private String nome;
	
	@Column(name = "cpf")
	@Getter @Setter private String cpf;
	
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Temporal(TemporalType.DATE)
	@Column(name = "data_nascimento")
	@Getter @Setter private Calendar dataNascimento;
	
	@Column(name = "endereco")
	@Getter @Setter private String endereco;
	
	@Column(name = "telefone_1", length = 11)
	@Getter @Setter private String telefone1;
	
	@Column(name = "telefone_2", length = 11)
	@Getter @Setter private String telefone2;
	
	public Cliente() {
		
	}

	public Cliente(String nome, String endereco, String telefone1) {
		this.nome = nome;
		this.endereco = endereco;
		this.telefone1 = telefone1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
}
