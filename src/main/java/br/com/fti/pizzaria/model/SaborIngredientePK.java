package br.com.fti.pizzaria.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class SaborIngredientePK implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Column(name = "_sabor")
	private Integer sabor;
	
	@Column(name = "_ingrediente")
	private Integer ingrediente;
	
	public SaborIngredientePK(Integer sabor, Integer ingrediente) {
		this.sabor = sabor;
		this.ingrediente = ingrediente;
	}
	
	public SaborIngredientePK() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ingrediente == null) ? 0 : ingrediente.hashCode());
		result = prime * result + ((sabor == null) ? 0 : sabor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaborIngredientePK other = (SaborIngredientePK) obj;
		if (ingrediente == null) {
			if (other.ingrediente != null)
				return false;
		} else if (!ingrediente.equals(other.ingrediente))
			return false;
		if (sabor == null) {
			if (other.sabor != null)
				return false;
		} else if (!sabor.equals(other.sabor))
			return false;
		return true;
	}
}
