package br.com.fti.pizzaria.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sabor_tamanho")
public class SaborTamanho implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SaborTamanhoPK id;
	
	@ManyToOne
	@JoinColumn(name = "_sabor", insertable = false, updatable = false)
	private Sabor sabor;
	
	@ManyToOne
	@JoinColumn(name = "_tamanho", insertable = false, updatable = false)
	private Tamanho tamanho;
	
	@Column(name = "valor", precision = 5, scale = 2)
	private BigDecimal valor;
	
	public SaborTamanho() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SaborTamanho other = (SaborTamanho) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
