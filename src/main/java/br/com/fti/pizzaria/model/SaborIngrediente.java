package br.com.fti.pizzaria.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sabor_ingrediente")
public class SaborIngrediente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private SaborIngredientePK id;
	
	@ManyToOne
	@JoinColumn(name="_sabor", insertable = false, updatable = false)
	private Sabor sabor;
	
	@ManyToOne
	@JoinColumn(name="_ingrediente", insertable = false, updatable = false)
	private Ingrediente ingrediente;
	
	public SaborIngrediente() {
		
	}
	
	public SaborIngrediente(Sabor sabor, Ingrediente ingrediente) {
		this.id = new SaborIngredientePK(sabor.getId(), 
				ingrediente.getId());
		this.sabor = sabor;
		this.ingrediente = ingrediente;
	}

}
