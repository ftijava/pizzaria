package br.com.fti.pizzaria.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "pizza_sabor")
public class PizzaSabor implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private PizzaSaborPK id;
	
	@ManyToOne
	@JoinColumn(name = "_pizza", insertable = false, updatable = false)
	private Pizza pizza;
	
	@ManyToOne
	@JoinColumn(name = "_sabor", insertable = false, updatable = false)
	private Sabor sabor;
	
	public PizzaSabor() {
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PizzaSabor other = (PizzaSabor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	

}
