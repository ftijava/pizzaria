package br.com.fti.pizzaria.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;


@Entity
@Table(name = "ingrediente")
public class Ingrediente implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter private Integer id;
	
	@Column(name = "descricao", nullable = false)
	@Getter private String descricao;
	
	@OneToMany(mappedBy = "ingrediente")
	private List<SaborIngrediente> saboresIngrediente;
	
	public Ingrediente() {
		
	}
	
	public Integer getId() {
		return id;
	}
	
	public Ingrediente(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}

}
