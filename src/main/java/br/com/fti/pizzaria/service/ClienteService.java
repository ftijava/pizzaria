package br.com.fti.pizzaria.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fti.pizzaria.exception.ValidacaoPreenchimentoException;
import br.com.fti.pizzaria.model.Cliente;
import br.com.fti.pizzaria.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository repository;
	
	public void salvar(Cliente cliente) {
		repository.save(cliente);
	}

	public List<Cliente> obterTodos() {
		return (List<Cliente>) repository.findAll();
	}
	
	/**
	 * metodo valida informacoes basicas do cliente
	 * @param cliente
	 * @return string vazia caso nao haja nada errado, 
	 * string com os erros caso contrario
	 */
	public String validarCliente(Cliente cliente) {
		StringBuilder validacao = new StringBuilder();
		
		if (cliente.getNome() ==null || cliente.getNome().length() < 2) {
			validacao.append("O campo nome deve conter mais do que 2 caracteres");
		}
		
		return validacao.toString();
	}
	
	public void validarCliente2(Cliente cliente) throws ValidacaoPreenchimentoException {
		StringBuilder validacao = new StringBuilder();
		if (cliente.getNome().length() < 2) {
			validacao.append("O campo nome deve conter mais do que 2 caracteres");
		}
		
		if (validacao.length() > 0) {
			throw new ValidacaoPreenchimentoException(validacao.toString());
		}
	}

	public Cliente obterPorId(Integer id) {
		return repository.findById(id);
	}
	
	public List<Cliente> obterPorNome(String nome) {
		return repository.findByNome(nome);
	}

	public void alterar(Cliente cliente) {
		//repository.
	}

}
