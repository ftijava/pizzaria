package br.com.fti.pizzaria.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.fti.pizzaria.model.Ingrediente;
import br.com.fti.pizzaria.repository.IngredienteRepository;

@Service
public class IngredienteService {
	
	@Autowired
	private IngredienteRepository repository;
	
	public Iterable<Ingrediente> obterTodos(){
		
		Iterable<Ingrediente> ingredientes = repository.findAll();
		
		return ingredientes;
	}
	
	
	public void salvar(Ingrediente Ingrediente){
		repository.save(Ingrediente);
	}

}
