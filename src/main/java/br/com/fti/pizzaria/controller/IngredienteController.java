package br.com.fti.pizzaria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fti.pizzaria.model.Ingrediente;
import br.com.fti.pizzaria.service.IngredienteService;

@Controller
public class IngredienteController {
	
	@Autowired
	private IngredienteService service;
	
	@RequestMapping("/")
	public String index(){
		return "index";
	}
	
	@RequestMapping("listaIngredientes")
	public String listaIngredientes(Model model){
		
		Iterable<Ingrediente> ingredientes = service.obterTodos();
		
		model.addAttribute("ingredientes", ingredientes);
		
		return "listaIngredientes";
	}
	
	@RequestMapping(value = "salvar", method = RequestMethod.POST )
	public String salvar(@RequestParam("descricao") String descricao, Model model){
		
		Ingrediente novoIngrediente = new Ingrediente(descricao);
		
		service.salvar(novoIngrediente);
		
		Iterable<Ingrediente> ingredientes = service.obterTodos();
		
		model.addAttribute("ingredientes", ingredientes);
		
		return "listaIngredientes";
	}

}
