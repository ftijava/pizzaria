package br.com.fti.pizzaria.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.fti.pizzaria.model.Cliente;
import br.com.fti.pizzaria.service.ClienteService;

@Controller
public class ClienteController {
	
	@Autowired
	private ClienteService service;
	
	@RequestMapping("inicializar")
	public String inicializar(Model model){
		listarClientes(model);
		return "clienteListagem";
	}
	
	@RequestMapping("irParaCadastroCliente")
	public String irParaCadastro(Model model){
		model.addAttribute("cliente", new Cliente());
		return "clienteCadastro";
	}
	
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable("id") Integer id, Model model) {
		Cliente cliente = service.obterPorId(id);
		return salvarCliente(cliente, model);
	}
	
	@RequestMapping("/salvarCliente")
	public String salvarCliente(Cliente cliente, Model model) {
		//valida com string
		String validacao = service.validarCliente(cliente);
//		if (validacao.isEmpty()) {
			//salvo
//		} else {
			//mensagem pra tela
//		}
		
		//valida com exception
//		try {
//			service.validarCliente2(cliente);
			//salvar
//		} catch (ValidacaoPreenchimentoException e) {
			//adicionar erro tela e.getMessage();
//		}
		
		if (cliente.getId() == null) {
			service.salvar(cliente);
		} else {
			service.alterar(cliente);
		}
		listarClientes(model);
		return "clienteListagem";
	}
	
	private void listarClientes(Model model) {
		List<Cliente> clientes = service.obterTodos();
		model.addAttribute("nomeesquisito", clientes);
	}
	
	@RequestMapping("buscarClientesPorNome")
	public String findCitiesNameEndsWith(Model model, @RequestParam String name) {
	    List<Cliente> clientes = service.obterTodos();
	    model.addAttribute("clientes", clientes);
	    return "clienteListagem";
	}
	
	@RequestMapping("editarCliente")
	public String  editarCliente(@PathVariable("id") Integer id, 
			Model model) {
		Cliente cliente = service.obterPorId(id);
		if (cliente != null) {
			model.addAttribute("cliente", cliente);
		} else {
			
		}
		return "clienteListagem" ;
	}

}
